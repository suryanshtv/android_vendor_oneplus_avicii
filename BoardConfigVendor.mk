# Automatically generated file. DO NOT MODIFY
#
# This file is generated by device/oneplus/avicii/setup-makefiles.sh

FIRMWARE_IMAGES := $(wildcard vendor/oneplus/avicii/firmware/*)

AB_OTA_PARTITIONS += \
    $(foreach f, $(notdir $(FIRMWARE_IMAGES)), $(basename $(f)))

